<?php
/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 2020/4/10
 * Time: 11:21
 */

namespace app\service\model;

use think\Model;

class Question extends Model
{
    protected $table = 'wolive_question';

    public static function getList()
    {
        $where = [];
        $limit = input('get.limit');
        if ($keyword = input('get.keyword')) $where['keyword'] = $keyword;
        $list = self::order('sort', 'asc')->where($where)->paginate($limit);
        return ['code' => 0, 'data' => $list->items(), 'count' => $list->total(), 'limit' => $limit];
    }
}