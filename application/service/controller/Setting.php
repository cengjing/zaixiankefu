<?php


namespace app\service\controller;

use think\Db;
use app\service\model\Service;

/**
 *
 * 后台页面控制器.
 */
class Setting extends Base
{
    public function sentence(){
        $login = $_SESSION['Msg'];
        $sentence = Db::table('wolive_sentence')->where(['service_id'=>$login['service_id']])->find();
        if($this->request->isAjax()){
            $post['content'] = $this->request->post('content','','\app\Common::clearXSS');

            if($login['level'] == 'super_manager'){
                $post['lang'] = $this->request->post('lang');
                if(!$post['lang']) $this->error("请选择默认语言");
                Db::table('wolive_business')->where(['id'=>$login['business_id']])->update(['lang' => $post['lang']]);
            }

            if($sentence){
                Db::table('wolive_sentence')->where(['service_id'=>$login['service_id']])->update(['content' => $post['content'],'state'=>'using']);
            }else{
                Db::table('wolive_sentence')->insert(['content'=>$post['content'],'service_id'=>$_SESSION['Msg']['service_id'],'state'=>'using']);
            }
            $this->success("保存成功");
        }
        $sentence = Db::name('wolive_sentence')->where(['service_id'=>$login['service_id']])->find();
        if($login['level'] == 'super_manager'){
            $business = Db::name('wolive_business')->where(['id'=>$login['business_id']])->find();
            $this->assign('business', $business);
        }
        $this->assign('sentence', $sentence);
        $this->assign('login', $login);
        return $this->fetch();
    }

    public function access(){
        $http_type = ((isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://';
        $web = $http_type . $_SERVER['HTTP_HOST'];
        $action = $web.request()->root();
        $login = $_SESSION['Msg'];
        $class = Db::table('wolive_group')->where('business_id', $login['business_id'])->select();
        $this->assign('class', $class);
        $this->assign('business', $login['business_id']);
        $this->assign('web', $web);
        $this->assign('login', $login);
        $this->assign('action', $action);
        $this->assign("title", "接入方法");
        $this->assign("part", "接入方法");
        return $this->fetch();
    }

    public function course(){
        $this->assign("service", Service::getService());
        $this->assign("domain", $this->request->domain());
        $this->assign("business_id", $_SESSION['Msg']['business_id']);
        return $this->fetch();
    }
}