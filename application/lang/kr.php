<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/9/16
 * Time: 17:17
 */

return [
    'hello' => '안녕하세요!',

    'service_offline' => '고객 서비스 오프라인 ',

    'service_online' => '온라인 고객 서비스 ',

    'say_is_off' => '세션이 닫혔습니다. ',

    'data_error' => '불완전한 데이터 ',

    'service_ban' => '상인의 고객 서비스는 이미 접근이 금지되었다',

    'service_leave' => '고객센터가 잠시 떠나니 메시지를 남겨주세요!다른 고객 서비스로 이동할 수도 있습니다. ',

    'service_empty' => '이 전용 고객 서비스는 존재하지 않습니다. ',

    'group_service_offline' => '이 범주의 고객 서비스가 온라인 상태가 아닙니다. ',

    'session_close' => '세션이 닫혔습니다. ',

    'save_file_error' => '스토리지 미디어 구성 정보를 확인하십시오. ',

    'offline' => '다른 쪽이 온라인 상태가 아닙니다.',

    'question_delete' => '문제가 제거되었습니다. ',

    'ext_error' => '이 파일 형식은 지원되지 않습니다. ',

    'illegal_img_error' => '불법 그림 파일 ',

    'evaluate_error' => '평가 실패, 다시 시도하십시오. ',

    'evaluate_thk' => '댓글 감사합니다.',

    'evaluate_score' => '점수를 매겨주세요. ',

    'mobile_error' => '핸드폰 형식이 정확하지 않다. ',

    'name_error' => '성함을 기입해 주십시오 ',

    'save_ok' => '제출 성공, 고객센터 회신 대기 ',

    'save_error' => '커밋 실패 ',

    'robot_error' => [

        '나는 너의 뜻을 잘 모르겠다.다른 질문을 시도해 보셔도 됩니다.',

        '나는 너의 문제를 잘 이해하지 못하지만, 나는 열심히 공부하고 있다',

        '이 문제 재미있어요. ',

        '나는 이미 작은 공책으로 너의 문제를 적어 두었다',

        '무슨 말인지 모르겠는데 설명해 주세요',

        '너무 빨리 물어봐.좀 쉬시고 다시 한 번 여쭤볼게요.',

    ],

    'send' => '우송',

    'please_enter' => '내용을 입력하십시오.',

    'ai_service' => 'AI 스마트 고객센터',

    'cancel' => '취소',

    'submit' => '제출',

    'evaluate_service' => '고객 서비스 평가',

    'transfer_service' => '다른 고객 서비스 부서로 이동하셨습니다.',

    'off_line' => '[오프라인]',

    'name' => '이름',

    'please_enter_name' => '이름을 입력하십시오.',

    'contact' => '연락처 정보',

    'please_enter_contact' => '연락처 정보를 입력하십시오.',

    'close_wav' => '프롬프트 닫기',

    'open_wav' => '프롬프트 사용하기',

    'paste_images_tip' => '잘라낸 그림을 입력란에 붙여넣기',

    'message_tip' => '소식 있어 ',

    'how_to_send_screenshot' => ' 스크린샷 ?',

    'ctrl_enter' => 'Enter 키를 눌러 메시지를 보내고 Ctrl+Enter 키를 눌러 줄을 바꿉니다.',

    'enter_ctrl' => 'Ctrl+enter 키를 눌러 메시지를 보내고 enter 키를 눌러 줄을 바꿉니다.',

    'disappointment' => '실망',

    'dissatisfaction' => '불만',
    'commonly' => '보통',

    'common' => '일반적',

    'satisfied' => '만족',

    'surprised' => '놀라움',
    'select' => '언어를 선택하십시오.',

    'please_select_images' => '그림을 선택하십시오',
    'not_supported' => '이 형식이 지원되지 않는 파일',
    'no_data' => '데이터 없음',
    'tip_waiting' => '알림: 대기 중',
    'tip' => '재촉',
    'is_transfer_service' => '이 클라이언트 서비스가 오프라인 상태인 경우 다른 클라이언트 서비스로 이전할지 여부?',
    'yes' => '그래',
    'no' => '아니요',
    'transferring' => '이동....',
    'guess_ask' => '물어보고 싶은가 봐요:',
    'please_enter_message' => '정보를 입력하십시오.',
];
