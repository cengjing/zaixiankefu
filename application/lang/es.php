<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/9/16
 * Time: 17:17
 */

return [
    'hello' => 'hola!',

    'service_offline' => 'el servicio al cliente está fuera de línea ',

    'service_online' => 'el servicio al cliente está en línea ',

    'say_is_off' => 'la sesión está cerrada ',

    'data_error' => 'datos incompletos ',

    'service_ban' => 'el servicio al cliente del comerciante está prohibido',

    'service_leave' => 'el servicio al cliente se va temporalmente, ¡ deje un mensaje! También puede transferirse a otros departamentos de servicio al cliente ',

    'service_empty' => 'Este servicio exclusivo al cliente no existe ',

    'group_service_offline' => 'Este tipo de servicio al cliente no está en línea ',

    'session_close' => 'la sesión está cerrada ',

    'save_file_error' => 'Compruebe la información de configuración del medio de almacenamiento ',

    'offline' => 'La otra parte no está en línea',

    'question_delete' => 'El problema ha sido eliminado ',

    'ext_error' => 'Este formato de archivo no es compatible ',

    'illegal_img_error' => 'Archivo de imagen ilegal ',

    'evaluate_error' => 'La evaluación falló, por favor vuelva a intentarlo ',

    'evaluate_thk' => 'Gracias por sus comentarios.',

    'evaluate_score' => 'Por favor, marque ',

    'mobile_error' => 'El formato del teléfono no es correcto ',

    'name_error' => 'Por favor, rellene su nombre. ',

    'save_ok' => 'Presentación exitosa, por favor espere la respuesta del servicio al cliente ',

    'save_error' => 'Presentación fallida ',

    'robot_error' => [

        'No te entiendo muy bien. Puedes probar otra pregunta',

        'No entiendo bien tu problema, pero estoy estudiando duro.',

        'Esta pregunta es interesante. ',

        'He anotado tu pregunta con un pequeño cuaderno.',

        'No te entiendo, describe.',

        'Preguntaste demasiado rápido. Por favor, descanse y pregunte de nuevo.',

    ],

    'send' => 'Correo',

    'please_enter' => 'Por favor, introduzca el contenido',

    'ai_service' => 'Servicio al cliente inteligente de Ia',

    'cancel' => 'Cancelación',

    'submit' => 'Presentación',

    'evaluate_service' => 'Evaluar el servicio al cliente',

    'transfer_service' => 'Ha sido transferido a otros departamentos de servicio al cliente',

    'off_line' => '[Fuera de línea]',

    'name' => 'Nombre',

    'please_enter_name' => 'Introduzca su nombre',

    'contact' => 'Información de contacto',

    'please_enter_contact' => 'Introduzca su información de contacto',

    'close_wav' => 'Cerrar el tono de alerta',

    'open_wav' => 'Activar el tono de alerta',

    'paste_images_tip' => 'Pegar la imagen cortada en el cuadro de entrada',

    'message_tip' => 'Tienes noticias. ',
    'select' => 'Por favor, elija el idioma',

    'how_to_send_screenshot' => ' Captura de pantalla?',

    'ctrl_enter' => 'Presione la tecla Enter para enviar el mensaje, Ctrl + enter para cambiar de línea',

    'enter_ctrl' => 'Presione la tecla Ctrl + enter para enviar el mensaje y presione la tecla Enter para cambiar de línea',

    'disappointment' => 'Decepción',

    'dissatisfaction' => 'Insatisfacción',
    'commonly' => 'same as',

    'common' => 'Típico',

    'satisfied' => 'Satisfecho',

    'surprised' => 'Sorpresa',

    'please_select_images' => 'Por favor, seleccione la imagen',
    'not_supported' => 'Archivos que no admiten este formato',
    'no_data' => 'Sin datos',
    'tip_waiting' => 'Aviso: cola para esperar',
    'tip' => 'Promover',
    'is_transfer_service' => 'quiere trasladar este servicio al cliente a otro servicio al cliente cuando está fuera de línea?',
    'yes' => 'Sí.',
    'no' => 'no',
    'transferring' => 'Traslado....',
    'guess_ask' => 'Creo que quieres preguntar.:',
    'please_enter_message' => 'Por favor, introduzca la información',
];
